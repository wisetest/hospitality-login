import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatePipe, CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import Swal from 'sweetalert2';
import { QRCodeComponent } from './qrcode/qrcode.component';
import { QRCodeModule } from 'angularx-qrcode';
import { TranslateService } from './translate.service';
import { TranslatePipe } from './translate.pipe';

export function setupTranslateFactory(
  service: TranslateService): Function {
    return () => service.use('ua');
  }
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    QRCodeComponent,
    TranslatePipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    QRCodeModule,
    CommonModule
  ],
  providers: [TranslateService,
  { provide: APP_INITIALIZER,
    useFactory : setupTranslateFactory,
    deps: [TranslateService],
    multi: true}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
