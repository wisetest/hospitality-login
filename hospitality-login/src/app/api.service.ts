import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class Sender {

  constructor(private httpClient: HttpClient) { }
  public postRegistrationInfo(req) {
    return this.httpClient.post<any>(`http://localhost:8011/login`, req);
    // 192.168.1.6
    // localhost
    // loyalty.kavalier.com.ua
  }

  public getCabinetInfo(email) {
    return  this.httpClient.get<any>(`http://localhost:8011/cabinet/cabuser/${email}`);

  }

  public changeCabinetData(req) {
    return this.httpClient.post<any>(`http://localhost:8011/cabinet/change/data`, req);
  }

  public sendMessage(req) {
    return this.httpClient.post<any>(`http://localhost:8011/cabinet/send/message`, req);
  }

  public getPropositions() {
    return this.httpClient.get<any>('http://localhost:8009/img/getthat');
  }
}
