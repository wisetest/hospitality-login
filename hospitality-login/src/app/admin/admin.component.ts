import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { Sender } from '../api.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from '../user';
import { DatePipe } from '@angular/common';
import {TranslateService} from '../translate.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [Sender]
})
export class AdminComponent implements OnInit {

  constructor(private authService: AuthService,
    private router: Router,
    private sender:     Sender,
    private fb: FormBuilder,
    private translate: TranslateService) {
      translate.use('ua').then(() => {
        console.log(translate.data);
      });
    }

  userId;
  person = {
    firstname :     'default',
    lastname :      'default',
    patronymic :    'default',
    birthdate :     '',
    mobilephone :   'default',
    email :         'default'
};

persondiscount = {
  group:          'default',
  code:           'default',
  discount:       'default',
  validity:       'default',
  bonussum:       'default',
  paysum:         'default',
  credit:         'default',
  accumulation:   'default',
  extramoneysum:  'default'
};

mesForm = new FormGroup({
Text: new FormControl('')
});

changeForm = new FormGroup({
  ID: new FormControl(''),
  FirstName:  new FormControl(''),
  LastName:   new FormControl(''),
  Patronymic: new FormControl(''),
  MobilePhone: new FormControl(''),
  EMail:      new FormControl(''),
  });


  setLangUA(lang: 'ua') {
    this.translate.use(lang);
    document.getElementById('patronymic').style.display = 'flex';
    document.getElementById('patronymic').style.flexWrap = 'wrap';
  }

  setLangEN(lang: 'en') {
    this.translate.use(lang);
    document.getElementById('patronymic').style.display = 'none';
  }

array;
element;
  ngOnInit() {
    this.sender.getPropositions().subscribe(index => {this.array = index })
  localStorage.getItem('ACCESS_TOKEN');
  this.sender.getCabinetInfo(localStorage.getItem('ACCESS_TOKEN')).subscribe( responseCabinetData => {
    this.userId = responseCabinetData.person.id;
    this.person.firstname = responseCabinetData.person.firstname;
      this.person.lastname = responseCabinetData.person.lastname;
      this.person.patronymic = responseCabinetData.person.patronymic;
      this.person.birthdate = responseCabinetData.person.birthdate;
      this.person.email = responseCabinetData.person.email;
      this.person.mobilephone = responseCabinetData.person.mobilephone;

      this.persondiscount.group = responseCabinetData.persondiscount.group;
      this.persondiscount.code = responseCabinetData.persondiscount.code;
      this.persondiscount.discount = responseCabinetData.persondiscount.discount;
      this.persondiscount.validity = responseCabinetData.persondiscount.validity;
      this.persondiscount.bonussum = responseCabinetData.persondiscount.bonussum;
      this.persondiscount.paysum = responseCabinetData.persondiscount.paysum;
      this.persondiscount.credit = responseCabinetData.persondiscount.credit;
      this.persondiscount.accumulation = responseCabinetData.persondiscount.accumulation;
      this.persondiscount.extramoneysum = responseCabinetData.persondiscount.extramoneysum;
  });
  }

  messageForm() {
    document.getElementById('mesForm').style.display= "block";
  }

  openForm() {
    document.getElementById('myForm').style.display = 'block';
    // document.getElementById('cover').style.display ='block';
  }

  closeForm() {
    document.getElementById('myForm').style.display = 'none';
    this.changeForm.reset();
  }

  closeMesForm() {
    document.getElementById('mesForm').style.display = 'none';
    this.mesForm.reset();
  }

sendMes() {
  this.sender.sendMessage(this.mesForm.value).subscribe(res => {
    console.log(33, res);
  })
}

sendNewData() {
  document.getElementById('myForm').style.display = 'none';
  this.changeForm.value.ID = this.userId;
  this.sender.changeCabinetData(this.changeForm.value).subscribe(response => {
    });
}

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
