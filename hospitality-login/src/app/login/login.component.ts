import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthService } from '../auth/auth.service';
import {Component, OnInit} from '@angular/core';
import { Sender } from '../api.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [Sender]
})

export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder , private sender: Sender) { }
  loginForm: FormGroup;
  isSubmitted  =  false;
  ngOnInit() {
    this.loginForm  =  this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get formControls() { return this.loginForm.controls; }

  login() {
      this.sender.postRegistrationInfo(this.loginForm.value).subscribe(response => {
        if (response === (true)) {
          this.authService.login(this.loginForm.value);
          this.router.navigateByUrl('/admin');
          } else {
            this.loginForm.reset();
            Swal.fire({
            type: 'error',
            title: 'Помилка',
            text: 'Неправильно введений пароль або емейл'
          });
          }
      });
    }
  }
